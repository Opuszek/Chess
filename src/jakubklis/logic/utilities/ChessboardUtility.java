package jakubklis.logic.utilities;

import java.util.ArrayList;
import java.util.HashMap;

import jakubklis.logic.classes.Field;
import jakubklis.logic.piece.pieces.Bishop;
import jakubklis.logic.piece.pieces.King;
import jakubklis.logic.piece.pieces.Knight;
import jakubklis.logic.piece.pieces.Pawn;
import jakubklis.logic.piece.pieces.PieceInterface;
import jakubklis.logic.piece.pieces.Queen;
import jakubklis.logic.piece.pieces.Rook;

public class ChessboardUtility {
	private ChessboardUtility () {
		
	}
	
	public static HashMap<Field,PieceInterface> createChessboard () {
		HashMap<Field,PieceInterface>chessboard = new HashMap<Field,PieceInterface>();
		for (int i = 1; i <= 8; i++) 
			for (int y = 1; y<= 8; y++) 
				chessboard.put(new Field(i,y), null);
		return chessboard;
	}
	
	public static HashMap<Field,PieceInterface> setPawns (HashMap<Field,PieceInterface> chessboard) {
		chessboard.forEach((k,v)->{
			if (k.getHorizontal()==2) chessboard.put(k, new Pawn(false)); 
			if (k.getHorizontal()==7) v = chessboard.put(k, new Pawn(true));
		});
		return chessboard;
		
	}
	
	public static HashMap<Field,PieceInterface> setPieces (HashMap<Field,PieceInterface> chessboard) {
		chessboard.forEach((k,v)->{
			if (k.getHorizontal()==1) {
				if(k.getVertical()==1 || k.getVertical()==8) chessboard.put(k, new Rook(false));
				if (k.getVertical()==2 || k.getVertical()==7) chessboard.put (k, new Knight(false));
				if (k.getVertical()==3 || k.getVertical()==6) chessboard.put (k, new Bishop(false));
				if (k.getVertical()==4) chessboard.put (k, new Queen(false));
				if (k.getVertical()==5) chessboard.put (k, new King(false));
				}
			if (k.getHorizontal()==8) {
				if(k.getVertical()==1 || k.getVertical()==8) chessboard.put(k, new Rook(true));
				if (k.getVertical()==2 || k.getVertical()==7) chessboard.put (k, new Knight(true));
				if (k.getVertical()==3 || k.getVertical()==6) chessboard.put (k, new Bishop(true));
				if (k.getVertical()==4) chessboard.put (k, new Queen(true));
				if (k.getVertical()==5) chessboard.put (k, new King(true));
			}
		});
		return chessboard;
	}
	
	public static boolean CheckValidField (Field field) {
		return ValidFieldHorizontal(field.getHorizontal()) && ValidFieldVertical (field.getVertical()); 
	}
	
	public static String toStringFieldList (ArrayList<Field>fieldList) {
		String result = "";
		int iterate = 1;
		Field loopField;
		for (int i = 0; i<fieldList.size();i++) {
			loopField = fieldList.get(i);
			result += iterate + ". Move to field of horizontal value " + loopField.getHorizontal() + 
					" and vertical value " + loopField.getVertical() + ".\n";
		}
		return result;
	}
	
	
	private static boolean  ValidFieldHorizontal (int horizontal) {
		return horizontal >=1 && horizontal <=8; 
	}
	
	private static boolean ValidFieldVertical (int vertical) {
		return vertical >=1 && vertical <=8;
	}
	
	
	public static String visualize (HashMap<Field,PieceInterface> chessboard) {
		String result = "";
		for (int x = 1; x <=8; x++) {
			for (int y = 1; y<=8; y++) {
				result += " " + chessboard.get (new Field (x,y));
				if (y==8) result += "\n";
			}
		}
		return result=="" ? "Chessboard is empty" : result;
	}
}
