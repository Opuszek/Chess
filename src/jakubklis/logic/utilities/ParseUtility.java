package jakubklis.logic.utilities;

import java.security.InvalidParameterException;

import jakubklis.logic.classes.Field;

public class ParseUtility {
    
	public static Field stringToField (String string) throws InvalidParameterException {
		isValid(string);
		int horizontal = Character.getNumericValue(string.charAt(0));
		int vertical = getVerticalValue (string.charAt(1));
		return new Field (horizontal,vertical);
	}
	
	private static void isValid (String string) {
	if (string.length()!=2) throw new InvalidParameterException ();
	if (!(Character.isDigit(string.charAt(0)))) throw new InvalidParameterException ();
	if (Character.getNumericValue(string.charAt(0))>8) throw new InvalidParameterException ();
	if (Character.getNumericValue(string.charAt(0))<1) throw new InvalidParameterException ();
	if (!(string.charAt(1) >= 'a' && string.charAt(1) <= 'h')) throw new InvalidParameterException ();
	}
	
	private static int getVerticalValue (char ch) {
		System.out.println("test");
		if (ch == 'a') return 1;
		if (ch == 'b') return 2;
		if (ch == 'c') return 3;
		if (ch == 'd') return 4;
		if (ch == 'e') return 5;
		if (ch == 'f') return 6;
		if (ch == 'g') return 7;
		if (ch == 'h') return 8;
		else throw new InvalidParameterException ();
	}
}
