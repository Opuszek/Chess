package jakubklis.logic.classes;

public class Field {
	int vertical;
	int horizontal;
	
	public Field (int horizontal,int vertical) {
		this.vertical = vertical;
		this.horizontal = horizontal;
	}
	
	public void setHorizontal (int horizontal){
		this.horizontal = horizontal;
	}
	public void setVertical (int vertical) {
		this.vertical = vertical;
	}
	
	public int getHorizontal (){
		return horizontal;
	}
	public int getVertical () {
		return vertical;
	}
	
	public boolean compareVertical (int vertical) {
		if (this.vertical!=vertical) return false;
		else return true;
	}
	
	public boolean compareHorizontal (int horizontal) {
		if (this.horizontal!=horizontal) return false;
		else return true;
	}
	
@Override
	public boolean equals (Object obj) {
		if (obj ==null) return false;
		if (!Field.class.isAssignableFrom(obj.getClass())) return false;
		final Field other = (Field) obj;
		if (this.vertical != other.vertical) return false;
		if (this.horizontal != other.horizontal) return false;
		return true;
}

@Override 
	public int hashCode () {
	return vertical * 100 + horizontal;
}

@Override
	public String toString() {
	char [] variables = new char[2];
	variables[0] = (Integer.toString(this.getHorizontal())).charAt(0);
	variables[1] = verticalPositionToChar(getVertical());
	return "Field " + variables[1] + variables[0];
}

	private char verticalPositionToChar (int vertical) {
		if (vertical==1) return 'A';
		if (vertical==2) return 'B';
		if (vertical==3) return 'C';
		if (vertical==4) return 'D';
		if (vertical==5) return 'E';
		if (vertical==6) return 'F';
		if (vertical==7) return 'G';
		if (vertical==8) return 'H';
		return '?';
	}
}
