package jakubklis.logic.classes;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.HashMap;

import jakubklis.logic.utilities.ChessboardUtility;
import jakubklis.logic.enumerator.EnumNextMove;
import jakubklis.logic.piece.movement.Movement;
import jakubklis.logic.piece.pieces.King;
import jakubklis.logic.piece.pieces.Pawn;
import jakubklis.logic.piece.pieces.PieceInterface;


public class Chessboard {
	
	HashMap<Field,PieceInterface>chessboard;
	
	public Chessboard (){
		chessboard = ChessboardUtility.createChessboard();
	}
	
	public void setPieces () {
		chessboard = ChessboardUtility.setPawns (chessboard);
		chessboard = ChessboardUtility.setPieces (chessboard);
	}
	
	public PieceInterface getPiece (Field field) {
		return chessboard.get(field);
	}
	
	public boolean isEmpty (Field field) {
		if (chessboard.get(field)==null) return true;
		return false;
		
	}
	public boolean isPawnWhite(Field field) {
		PieceInterface piece = getPieceFromField(field);
		return piece.isWhite();
	}
	
	public boolean areWhiteVictorious () {
		return !(chessboard.containsValue(new King(true)));
	}
	
	public boolean areBlackVictorious () {
		return !(chessboard.containsValue(new King(false)));
	}
	
	public ArrayList<Field> checkMovements (Field field){
		PieceInterface piece = getPieceFromField(field);
		if (piece.hasContinuousMovement()) return checkContinuousMovements (field,piece);
		else return checkSingularMovements (field,piece);
	}
	
	
	public void movePiece (Field startField, Field targetField) {
		PieceInterface piece = getPieceFromField (startField);
		if ( piece== null) throw new InvalidParameterException();
		setPieceOnField (null, startField);
		setPieceOnField (piece,targetField);
		
	}
	
	public boolean hasPiece (Field field) {
		return chessboard.get(field)!=null;
	}
	
	private ArrayList<Field> checkSingularMovements (Field field, PieceInterface piece) {
		ArrayList<Field>fieldList = new ArrayList<Field>();
		ArrayList<Movement>movements = new ArrayList<Movement>(); 
		if (piece instanceof Pawn) movements = computeMovementsForPawn ((Pawn) piece,field);
		else movements= piece.getMovements();
		movements.forEach((k)-> {
			Field targetField = switchField(field,k);
			if (!(checkIfAvailableMove(piece,targetField)==EnumNextMove.UNAVAILABLE)) fieldList.add(targetField);
		});
		return fieldList;
	}
	
	private ArrayList<Movement> computeMovementsForPawn(Pawn piece, Field field) {
		ArrayList<Movement>movementList = new ArrayList<Movement> ();
		boolean isWhite = piece.isWhite();
		int pieceUpwardMovement = isWhite ? -1 : 1; 
		PieceInterface leftPiece = getPieceFromField(moveField(field,pieceUpwardMovement,-1));
		PieceInterface rightPiece = getPieceFromField(moveField(field,pieceUpwardMovement,1));
		PieceInterface beforePiece = getPieceFromField(moveField(field,pieceUpwardMovement,0));
		PieceInterface firstMovePiece = getPieceFromField(moveField(field,pieceUpwardMovement * 2,0));
		if (leftPiece!=null && isWhite!=leftPiece.isWhite())
			movementList.addAll(piece.getSlantedLeftMovement());
		if (rightPiece!=null && isWhite!=rightPiece.isWhite())
			movementList.addAll(piece.getSlantedRightMovement());
		if (piece.isWhite()==true ? field.getHorizontal()==7 : field.getHorizontal()==2 && firstMovePiece==null )
			movementList.addAll(piece.getFirstMoveMovements());
		if (beforePiece==null)
			movementList.addAll(piece.getMovements());
		return movementList;
		
	}

	
	private Field moveField (Field field, int horizontal, int vertical) {
		Field resultField = new Field (field.getHorizontal() + horizontal, field.getVertical() + vertical);
		return resultField;
	}

	private ArrayList<Field> checkContinuousMovements(Field field, PieceInterface piece){
		ArrayList<Field>fieldList = new ArrayList<Field>();
		ArrayList<Movement>movements = new ArrayList<Movement>();
		movements= piece.getMovements();
		movements.forEach((k)-> {
			Field loopField = field;
			while(true) {
			loopField = switchField(loopField,k);
			EnumNextMove enumMove= checkIfAvailableMove(piece,loopField);
			if (enumMove!=EnumNextMove.UNAVAILABLE) fieldList.add(loopField);
			if(enumMove!=EnumNextMove.FREE) break;
			}
		});
		return fieldList;
	}
	
	private Field switchField (Field field, Movement movement) {
		int horizontal = field.getHorizontal() + movement.getHorizontal();
		int vertical = field.getVertical() + movement.getVertical();
		return new Field (horizontal,vertical);
	}
	
	private EnumNextMove checkIfAvailableMove (PieceInterface piece, Field targetField) {
		 PieceInterface targetPiece = getPieceFromField(targetField);
		 if (!ChessboardUtility.CheckValidField(targetField)) return EnumNextMove.UNAVAILABLE;
		 if (targetPiece==null) return EnumNextMove.FREE;
		 if (targetPiece.isWhite()!=piece.isWhite()) return EnumNextMove.LAST;
		 return EnumNextMove.UNAVAILABLE;
	}
	
	private PieceInterface getPieceFromField (Field field) {
		return chessboard.get(field);
	}
	
	private void setPieceOnField (PieceInterface piece, Field field) {
		chessboard.put(field, piece);
	}
	
	private boolean checkIfFirstPawnMove (PieceInterface piece, Field field) {
		if (!(piece instanceof Pawn)) return false;
		if (piece.isWhite()==true ? field.getHorizontal()!=7 : field.getHorizontal()!=2 ) return false;
		return true;
	}
	

	
	
}
	
