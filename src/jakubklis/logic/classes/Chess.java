package jakubklis.logic.classes;

import java.util.ArrayList;

import jakubklis.logic.piece.pieces.PieceInterface;

public class Chess {
	private Chessboard chessboard;
	private boolean isWhiteMove;

	public Chess() {
		chessboard = new Chessboard();
		chessboard.setPieces();
		isWhiteMove = true;
	}

	public ArrayList<Field> checkMovements(Field field) {
		return chessboard.checkMovements(field);
	}

	public void movePiece(Field moveField, Field targetField) {
		chessboard.movePiece(moveField, targetField);
		switchTour();
	}

	public boolean isMoveAvailable(Field field) {
		return chessboard.isPawnWhite(field) == isWhiteMove;
	}

	public PieceInterface getPiece(Field field) {
		return chessboard.getPiece(field);
	}

	public boolean isEmpty(Field field) {
		return chessboard.isEmpty(field);
	}

	public boolean isPawnWhite(Field field) {
		return chessboard.isPawnWhite(field);
	}

	public boolean areWhiteVictorious() {
		return chessboard.areWhiteVictorious();
	}

	public boolean areBlackVictorious() {
		return chessboard.areBlackVictorious();
	}

	private void switchTour() {
		isWhiteMove = !isWhiteMove;
	}

}
