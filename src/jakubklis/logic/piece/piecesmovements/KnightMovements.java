package jakubklis.logic.piece.piecesmovements;
import java.util.ArrayList;

import jakubklis.logic.piece.movement.Movement;
import jakubklis.logic.piece.movement.Movements;

public class KnightMovements implements Movements {
	ArrayList<Movement> movements;
	public KnightMovements () {
		movements = new ArrayList<Movement>();
		movements.add (new Movement (2,1));
		movements.add (new Movement (2,-1));
		movements.add (new Movement (1,2));
		movements.add (new Movement (-1,2));
		movements.add (new Movement (1,-2));
		movements.add (new Movement (-1,-2));
		movements.add (new Movement (-2,1));
		movements.add (new Movement (-2,-1));
	}
	
	public ArrayList<Movement> getList () {
		return movements;
	}
	

	

	
}