package jakubklis.logic.piece.piecesmovements;

import java.util.ArrayList;

import jakubklis.logic.piece.movement.Movement;
import jakubklis.logic.piece.movement.Movements;

public class QueenMovements implements Movements {
	ArrayList<Movement> movements;
	public QueenMovements () {
		movements = new ArrayList<Movement>();
		movements.add (new Movement (1,1));
		movements.add (new Movement (1,-1));
		movements.add (new Movement (-1,1));
		movements.add (new Movement (-1,-1));
		movements.add (new Movement (0,1));
		movements.add (new Movement (1,0));
		movements.add (new Movement (-1,0));
		movements.add (new Movement (0,-1));
	}
	
	public ArrayList<Movement> getList () {
		return movements;
	}
	


	
}