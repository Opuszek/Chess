package jakubklis.logic.piece.piecesmovements;

import java.util.ArrayList;

import jakubklis.logic.piece.movement.Movement;
import jakubklis.logic.piece.movement.Movements;

public class BishopMovements implements Movements {
	ArrayList<Movement> movements;
	public BishopMovements () {
		movements = new ArrayList<Movement>();
		movements.add (new Movement (1,1));
		movements.add (new Movement (1,-1));
		movements.add (new Movement (-1,1));
		movements.add (new Movement (-1,-1));
	}
	
	public ArrayList<Movement> getList () {
		return movements;
	}
	
}