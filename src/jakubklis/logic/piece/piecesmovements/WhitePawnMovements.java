package jakubklis.logic.piece.piecesmovements;

import java.util.ArrayList;

import jakubklis.logic.piece.movement.Movement;
import jakubklis.logic.piece.movement.Movements;

public class WhitePawnMovements implements Movements {
	ArrayList<Movement> movements;
	ArrayList<Movement> firstmovement;
	ArrayList<Movement> slantRightMovement;
	ArrayList<Movement> slantLeftMovement;
	public WhitePawnMovements () {
		movements = new ArrayList<Movement>();
		movements.add (new Movement (-1,0));
		firstmovement = new ArrayList<Movement>();
		firstmovement.add (new Movement (-2,0));
		firstmovement.add (new Movement (-1,0));
		slantLeftMovement = new ArrayList<Movement>();
		slantLeftMovement.add(new Movement(-1,-1));
		slantRightMovement = new ArrayList<Movement>();
		slantRightMovement.add(new Movement(-1,1));
	}
	
	public ArrayList<Movement> getList () {
		return movements;
	}
	
	public ArrayList<Movement> getFirstMovementList() {
		return firstmovement;
	}

	
	public String toString () {
		return "PawnMovements";
	}
	
	public ArrayList<Movement> getRightSlantMovement() {
		return slantRightMovement;
	}
	
	public ArrayList<Movement> getLeftSlantMovement() {
		return slantLeftMovement;
	}
	
}
