package jakubklis.logic.piece.movement;

public class Movement {
	Integer [] movement;
	public Movement (int horizontal, int vertical){
		movement = new Integer [2];
		movement [0] = horizontal;
		movement [1] = vertical;
		}
		
		public int getHorizontal () {
		return movement [0];
	}
		
		public int getVertical () {
			return movement [1];
		}
	
}