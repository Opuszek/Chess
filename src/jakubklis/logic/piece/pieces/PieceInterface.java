package jakubklis.logic.piece.pieces;

import java.util.ArrayList;

import jakubklis.logic.piece.movement.Movement;

public interface PieceInterface {
	public boolean isWhite ();
	public ArrayList<Movement> getMovements();
	public boolean hasContinuousMovement();
}
