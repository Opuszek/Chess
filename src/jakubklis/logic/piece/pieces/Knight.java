package jakubklis.logic.piece.pieces;

import java.util.ArrayList;

import jakubklis.logic.piece.movement.Movement;
import jakubklis.logic.piece.movement.Movements;
import jakubklis.logic.piece.piecesmovements.KnightMovements;

public class Knight implements PieceInterface {
	
	boolean isWhite;
	Movements movements;
	
	public Knight (boolean isWhite){
	this.isWhite = isWhite;
	movements = new KnightMovements();
	}
	
	public boolean isWhite () {
		return isWhite;
	}
	
	public ArrayList<Movement> getMovements () {
		return movements.getList();
	}
	
	@Override
	public String toString () {
	if (isWhite==true) return "White King";
	else return "Black King";
}

	public boolean hasContinuousMovement() {
		return false;
	}
}
