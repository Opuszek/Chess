package jakubklis.logic.piece.pieces;

import java.util.ArrayList;

import jakubklis.logic.piece.piecesmovements.BishopMovements;
import jakubklis.logic.piece.movement.Movement;
import jakubklis.logic.piece.movement.Movements;

public class Bishop implements PieceInterface {
	
	boolean isWhite;
	Movements movements;
	
	public Bishop (boolean isWhite){
	this.isWhite = isWhite;	
	movements = new BishopMovements();
	}
	
	public boolean isWhite () {
		return isWhite;
	}
	
	public ArrayList<Movement> getMovements () {
		return movements.getList();
	}
	
	@Override
	public String toString () {
	if (isWhite==true) return "White Bishop";
	else return "Black Bishop";
}
	
	public boolean hasContinuousMovement() {
		return true;
	}

}
