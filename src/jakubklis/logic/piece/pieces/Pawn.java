package jakubklis.logic.piece.pieces;

import java.util.ArrayList;

import jakubklis.logic.piece.piecesmovements.BlackPawnMovements;
import jakubklis.logic.piece.piecesmovements.WhitePawnMovements;
import jakubklis.logic.piece.movement.Movement;
import jakubklis.logic.piece.movement.Movements;

public class Pawn implements PieceInterface {
	
	boolean isWhite;
	Movements movements;
	
	public Pawn (boolean isWhite) {
		this.isWhite = isWhite;
		if (isWhite==true) movements = new WhitePawnMovements ();
		else movements = new BlackPawnMovements();
		
	}
	
	public boolean isWhite () {
		return isWhite;
	}

@Override
	public String toString () {
	if (isWhite==true) return "White Pawn";
	else return "Black Pawn";
}
@Override 
	public boolean equals (Object obj) {
	if (obj == null) return false;
	if (!Pawn.class.isAssignableFrom(obj.getClass())) return false;
	Pawn other = (Pawn) obj;
	if (other.isWhite()!=this.isWhite()) return false;
	return true;
}


public ArrayList<Movement> getMovements() {
	return movements.getList();
}

public ArrayList<Movement> getFirstMoveMovements () {
	if (isWhite==true) return ((WhitePawnMovements) movements).getFirstMovementList();
	else return ((BlackPawnMovements) movements).getFirstMovementList();
}

public ArrayList<Movement> getSlantedRightMovement () {
	if (isWhite==true) return ((WhitePawnMovements) movements).getRightSlantMovement();
	else return ((BlackPawnMovements) movements).getRightSlantMovement();
}

public ArrayList<Movement> getSlantedLeftMovement () {
	if (isWhite==true) return ((WhitePawnMovements) movements).getLeftSlantMovement();
	else return ((BlackPawnMovements) movements).getLeftSlantMovement();
}


public boolean hasContinuousMovement() {
	return false;
}




}
