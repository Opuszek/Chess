package jakubklis.logic.piece.pieces;

import java.util.ArrayList;

import jakubklis.logic.piece.piecesmovements.QueenMovements;
import jakubklis.logic.piece.movement.Movement;
import jakubklis.logic.piece.movement.Movements;

public class Queen implements PieceInterface {
	
	boolean isWhite;
	Movements movements;
	
	public Queen (boolean isWhite){
	this.isWhite = isWhite;	
	movements = new QueenMovements ();
	}
	
	public boolean isWhite () {
		return isWhite;
	}
	
	public ArrayList<Movement> getMovements () {
		return movements.getList();
	}
	
@Override
	public String toString () {
	if (isWhite==true) return "White Queen";
	else return "Black Queen";
}

public boolean hasContinuousMovement () {
	return true;
}

}
