package jakubklis.logic.piece.pieces;

import java.util.ArrayList;

import jakubklis.logic.piece.piecesmovements.KingMovements;
import jakubklis.logic.piece.movement.Movement;
import jakubklis.logic.piece.movement.Movements;

public class King implements PieceInterface {
	
	boolean isWhite;
	Movements movements;
	
	public King (boolean isWhite){
	this.isWhite = isWhite;
	movements = new KingMovements();
	}
	
	public boolean isWhite () {
		return isWhite;
	}
	
	public ArrayList<Movement> getMovements () {
		return movements.getList();
	}
	
@Override
	public String toString () {
	if (isWhite==true) return "White King";
	else return "Black King";
}

@Override 
	public boolean equals(Object obj) {
	if (!(obj instanceof King)) return false;
	King other = (King) obj;
	return other.isWhite()==this.isWhite(); 
}

	public boolean hasContinuousMovement() {
		return false;
	}
	
}
