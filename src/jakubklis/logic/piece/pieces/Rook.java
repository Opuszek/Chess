package jakubklis.logic.piece.pieces;

import java.util.ArrayList;

import jakubklis.logic.piece.piecesmovements.RookMovements;
import jakubklis.logic.piece.movement.Movement;
import jakubklis.logic.piece.movement.Movements;

public class Rook implements PieceInterface {
	
	boolean isWhite;
	Movements movements;
	
	public Rook (boolean isWhite){
	this.isWhite = isWhite;
	movements = new RookMovements();
	}
	
	public boolean isWhite () {
		return isWhite;
	}
	
	public ArrayList<Movement> getMovements () {
		return movements.getList();
	}
	
@Override
	public String toString () {
	if (isWhite==true) return "White Rook";
	else return "Black Rook";
}

	public boolean hasContinuousMovement () {
		return true;
	}
}

