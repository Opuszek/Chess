package jakubklis.utilities;

import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class PiecesImageCache {
	Image blackPawn;
	Image blackRook;
	Image blackKnight;
	Image blackBishop;
	Image blackQueen;
	Image blackKing;
	Image whitePawn;
	Image whiteRook;
	Image whiteKnight;
	Image whiteBishop;
	Image whiteQueen;
	Image whiteKing;
	
	public PiecesImageCache () {
		importImages();
	}
	
	public Image getBlackPawn () {
		return blackPawn;
	}
	
	public Image getBlackRook () {
		return blackRook;
	}
	
	public Image getBlackKnight() {
		return blackKnight;
	}
	
	public Image getBlackBishop () {
		return blackBishop;
	}
	
	public Image getBlackQueen() {
		return blackQueen;
	}
	
	public Image getBlackKing () {
		return blackKing;
	}
	
	public Image getWhitePawn () {
		return whitePawn;
	}
	
	public Image getWhiteRook () {
		return whiteRook;
	}
	public Image getWhiteKnight() {
		return whiteKnight;
	}
	
	public Image getWhiteBishop () {
		return whiteBishop;
	}
	
	public Image getWhiteQueen () {
		return whiteQueen;
	}
	
	public Image getWhiteKing () {
		return whiteKing;
	}
	
	private void importImages () {
		blackPawn = getImage ("BlackPawn");
		blackRook = getImage ("BlackRook");
		blackKnight = getImage ("BlackKnight");
		blackBishop = getImage ("BlackBishop");
		blackQueen = getImage ("BlackQueen");
		blackKing = getImage ("BlackKing");
		whitePawn = getImage ("WhitePawn");
		whiteRook = getImage ("WhiteRook");
		whiteKnight = getImage ("WhiteKnight");
		whiteBishop = getImage ("WhiteBishop");
		whiteQueen = getImage ("WhiteQueen");
		whiteKing = getImage ("WhiteKing");
	}
	
	private Image getImage(String name) {
		Image pawn = null;
		try {
			pawn = ImageIO.read(new File("Images/" + name + ".png"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return GraphicUtilities.getScaledImage(pawn,40,30);
	}
}
