package jakubklis.utilities;

import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class ImageFabric {
	
	public ImageFabric () {}
	
	public static Image getVictoryScreen (int horizontal, int vertical) {
		Image victoryScreen = null;
		try {
			victoryScreen = ImageIO.read(new File("Images/VictoryScreen.jpg"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return GraphicUtilities.getScaledImage(victoryScreen,horizontal,vertical);
	}

}
