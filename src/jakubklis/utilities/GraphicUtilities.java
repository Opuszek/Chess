package jakubklis.utilities;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;

public class GraphicUtilities {
	public static Image getScaledImage(Image srcImg, int w, int h){
	    BufferedImage resizedImg = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
	    Graphics2D g2 = resizedImg.createGraphics();

	    g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
	    g2.drawImage(srcImg, 0, 0, w, h, null);
	    g2.dispose();

	    return resizedImg;
	}
	
	public static Image invertImage (Image image) {
		BufferedImage bufferedImage = toBufferedImage(image);
		for (int x = 0; x < bufferedImage .getWidth(); x++) {
            for (int y = 0; y < bufferedImage .getHeight(); y++) {
                int rgba = bufferedImage .getRGB(x, y);
                Color col = new Color(rgba, true);
                col = new Color(255 - col.getRed(),
                                255 - col.getGreen(),
                                255 - col.getBlue());
                bufferedImage.setRGB(x, y, col.getRGB());
            }
        }
		return bufferedImage;
	}
	
	public static Image invertImageTest (Image image) {
		BufferedImage bufferedImage = toBufferedImage(image);
		for (int x = 0; x < bufferedImage .getWidth(); x++) {
            for (int y = 0; y < bufferedImage .getHeight(); y++) {
                int rgba = bufferedImage .getRGB(x, y);
                Color col = new Color(rgba);
                col = new Color(255 - col.getRed(),
                                255 - col.getGreen(),
                                255 - col.getBlue());
                bufferedImage.setRGB(x, y, col.getRGB());
            }
        }
		return bufferedImage;
	}
	
	public static BufferedImage toBufferedImage(Image img)
	{
	    if (img instanceof BufferedImage)
	    {
	        return (BufferedImage) img;
	    }

	    // Create a buffered image with transparency
	    BufferedImage bimage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);

	    // Draw the image on to the buffered image
	    Graphics2D bGr = bimage.createGraphics();
	    bGr.drawImage(img, 0, 0, null);
	    bGr.dispose();

	    // Return the buffered image
	    return bimage;
	}
}
