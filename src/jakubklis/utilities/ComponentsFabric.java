package jakubklis.utilities;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

public class ComponentsFabric {

	public static void addVictoryLabel (Container pane) {
		JLabel victoryLabel = new JLabel("Victory Screen", SwingConstants.CENTER);
		victoryLabel.setHorizontalTextPosition(JLabel.CENTER);
		victoryLabel.setBackground (Color.WHITE);
		victoryLabel.setForeground (Color.RED);
		victoryLabel.setFont(new Font("Courier New", Font.BOLD, 30));
		victoryLabel.setOpaque(true);
		pane.add(victoryLabel, BorderLayout.CENTER);
        victoryLabel.setIcon(new ImageIcon (ImageFabric.getVictoryScreen(pane.getHeight(), pane.getWidth())));
	}
	
}
