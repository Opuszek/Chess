package jakubklis.pane;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JButton;

public class DarkBrownField extends JField {
	
	private static final long serialVersionUID = 1L;
	final private Color DefaultColor = new Color (139,75,11);
	final private Color MarkedColor = new Color (215,131,48);
	final private Color SelectedColor = new Color (255,158,53);

	DarkBrownField (int horizontal, int vertical) {
		super(horizontal, vertical);
		this.setBackground(DefaultColor);
		setListenersChangingColor();
		deleteBorder();
		}
	
	public void selectColor() {this.setBackground(SelectedColor);}
	
	public void changeColorToDefault() {this.setBackground(DefaultColor);}
	
	public void markColor() {this.setBackground(MarkedColor);}
	
	private void setListenersChangingColor () {
		this.addMouseListener(new java.awt.event.MouseAdapter() {
		    public void mouseEntered(java.awt.event.MouseEvent evt) {
		    	if (DarkBrownField.this.getBackground().equals(DefaultColor))
		    	DarkBrownField.this.selectColor();
		    }
		    public void mouseExited(java.awt.event.MouseEvent evt) {
		    	if (DarkBrownField.this.getBackground().equals(SelectedColor))
		    	  DarkBrownField.this.changeColorToDefault();
		    }});
	}
	
	private void deleteBorder () {
		this.setBorder(BorderFactory.createEmptyBorder());
	}
	
}