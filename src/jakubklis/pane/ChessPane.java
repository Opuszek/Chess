package jakubklis.pane;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import jakubklis.logic.classes.Chess;
import jakubklis.logic.classes.Field;
import jakubklis.utilities.ComponentsFabric;
import jakubklis.utilities.PiecesImageCache;

public class ChessPane extends JFrame {
	

	private static final long serialVersionUID = 1L;
	private ArrayList<JField> jFieldList;
	private PiecesImageCache imageCache;
	private JField selectedJField;
	private JLabel mouseListener;
	private Chess chess;
	
	public ChessPane (String name) {
		super(name);
		setResizable(false);
		chess = new Chess();
		imageCache = new PiecesImageCache();
		createFieldList();
	}
	
	
	
	private static  void createAndShowGUI() {
        ChessPane frame = new ChessPane("Chess");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.addComponentsToPane(frame.getContentPane());
        frame.pack();
        frame.setVisible(true);
    }
	
	private void createFieldList () {
		jFieldList = new ArrayList<JField>();
		createEmptyFields();
		setBlackPawns();
		setWhitePawns();
	}
	
	public void addComponentsToPane (final Container pane ) {
		JPanel main = new JPanel();
		main.setLayout(new BoxLayout(main, BoxLayout.X_AXIS));
		JPanel chessAndBottomGui = new JPanel();
		 chessAndBottomGui.setLayout(new BoxLayout(chessAndBottomGui, BoxLayout.Y_AXIS));
		JPanel chessPanel = new JPanel ();
		JPanel bottomGUI = new JPanel ();
		JPanel mainPanel = new JPanel ();
		JLabel bottomGUIText = new JLabel("Chess",SwingConstants.LEFT);
		bottomGUI.add(bottomGUIText);
		mouseListener = bottomGUIText;
		chessPanel.setLayout(new GridLayout(8,8));
        jFieldList.forEach ((k)->{chessPanel.add(k);});
        chessAndBottomGui.add(chessPanel);
        chessAndBottomGui.add(bottomGUI);
        chessAndBottomGui.setBorder(BorderFactory.createRaisedBevelBorder());
        mainPanel.add(chessAndBottomGui);
        pane.add(mainPanel, BorderLayout.CENTER);
	}
	
	public void showVictoryScreen () {
		final Container pane = this.getContentPane();
		pane.removeAll();
		ComponentsFabric.addVictoryLabel(pane);
		this.setSize(500,500);
		setLocationRelativeTo(null);
		this.revalidate();
		this.repaint(); 
	}
	
	public static void main(String[] args) {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
	}	
	
	private void changeMouseListener (Object obj) {
		mouseListener.setText(obj.toString());
	}
	
	private void createEmptyFields () {
		for (int i = 0; i<32; i++) {
        	if ((i/4) % 2 == 0) {
        	jFieldList.add(addListener(new LightBrownField((i/4)+1,((i*2)% 8) + 1)));
        	jFieldList.add(addListener(new DarkBrownField((i/4)+1,((i*2)% 8) + 2)));
        	}
        	else {
        	jFieldList.add(addListener(new DarkBrownField((i/4)+1,((i*2)% 8) + 1)));
        	jFieldList.add(addListener (new LightBrownField((i/4)+1,((i*2)% 8) + 2)));
        	}
		}
	}
	
	private void setBlackPawns () {
		jFieldList.forEach((k)->{
			if (k.getHorizontal()==2) {
				k.setIcon (new ImageIcon(imageCache.getBlackPawn()));
				}
			if (k.getHorizontal()==1) {
				if (k.getVertical()==8 || k.getVertical()==1) k.setIcon(new ImageIcon(imageCache.getBlackRook()));
				if (k.getVertical()==7 || k.getVertical()==2) k.setIcon(new ImageIcon(imageCache.getBlackKnight()));
				if (k.getVertical()==6 || k.getVertical()==3) k.setIcon(new ImageIcon(imageCache.getBlackBishop()));
				if (k.getVertical()==4) k.setIcon(new ImageIcon(imageCache.getBlackQueen()));
				if (k.getVertical()==5) k.setIcon(new ImageIcon(imageCache.getBlackKing()));
			}
		});
	}
	
	private void setWhitePawns () {
		jFieldList.forEach((k)->{
			if (k.getHorizontal()==7) {
				k.setIcon (new ImageIcon(imageCache.getWhitePawn()));
				}
			if (k.getHorizontal()==8) {
				if (k.getVertical()==8 || k.getVertical()==1) k.setIcon(new ImageIcon(imageCache.getWhiteRook()));
				if (k.getVertical()==7 || k.getVertical()==2) k.setIcon(new ImageIcon(imageCache.getWhiteKnight()));
				if (k.getVertical()==6 || k.getVertical()==3) k.setIcon(new ImageIcon(imageCache.getWhiteBishop()));
				if (k.getVertical()==4) k.setIcon(new ImageIcon(imageCache.getWhiteQueen()));
				if (k.getVertical()==5) k.setIcon(new ImageIcon(imageCache.getWhiteKing()));
			}
			
		});
		  
	}
	
	private JField addListener (JField jField) {
		jField.addActionListener(new  ActionListener() {
			@Override
			public void actionPerformed (ActionEvent actionEvent) {
				JField insideField = null;
				Object source = actionEvent.getSource();
				if (source instanceof JField) insideField = (JField) source;
				addActionRightClickOnNull (insideField);
				addActionRightClickToSelect (insideField);
				if (areBlackVictorious()) showVictoryScreen();
				if (areWhiteVictorious()) showVictoryScreen();
				}
		});
		jField.addMouseListener(new java.awt.event.MouseAdapter() {
		   	public void mouseEntered(java.awt.event.MouseEvent mouseEvent) {
				JField insideField = null;
				Object source = mouseEvent.getSource();
				if (source instanceof JField) insideField = (JField) source;
				Field field = convertJFieldtoField(insideField);
				if (chess.isEmpty(field)) changeMouseListener(field);
				else changeMouseListener(chess.getPiece(field));
			   }
		   });
		return jField;
	}
	
	
	
	private void addActionRightClickToSelect (JField insideField) {
			if (insideField.getIcon()!=null && chess.isMoveAvailable(convertJFieldtoField(insideField))) {
				if (selectedJField==null){
			selectedJField=insideField;
			selectJFields(getMoveableJFields(insideField));}
			}
	}
	
	
	private void addActionRightClickOnNull (JField insideField) {
					if (selectedJField!=null)  {
						ArrayList<JField> moveableJFields = getMoveableJFields(selectedJField);
						if (moveableJFields.contains(insideField)) {
							movePiece(selectedJField,insideField);
						}
						cancelSelection(moveableJFields);
				}
			}
	
	private void movePiece (JField jFieldMove, JField jFieldTarget) {
		moveIcon (jFieldMove, jFieldTarget);
		moveImplementation (convertJFieldtoField(jFieldMove),convertJFieldtoField(jFieldTarget));
	}
	
	private void moveIcon (JField jFieldMove, JField jFieldTarget) {
		jFieldTarget.setIcon(jFieldMove.getIcon());
		jFieldMove.setIcon(null);
	}
	
	private void moveImplementation (Field fieldMove, Field targetField) {
		chess.movePiece (fieldMove, targetField);
	}
	
	private Field convertJFieldtoField (JField jField) {
		return new Field (jField.getHorizontal(),jField.getVertical());
	}
	
	private JField convertFieldtoJField (Field field) {
		return new JField (field.getHorizontal(),field.getVertical());
	}
	
	private ArrayList<JField> convertFieldListToJFieldList (ArrayList<Field>fieldList) {
		ArrayList<JField>jFieldList = new ArrayList<JField>();
		fieldList.forEach((k)->{jFieldList.add(convertFieldtoJField(k));});
		return jFieldList;
	}
	
	private ArrayList<JField> getMoveableJFields (JField Jfield) {
		Field field = convertJFieldtoField (Jfield);
		ArrayList<Field>fieldList = chess.checkMovements(field);
		return convertFieldListToJFieldList (fieldList);
	} 
	
	private void selectJFields (ArrayList<JField>jFieldList) {
		this.jFieldList.forEach((k)->{
			if (jFieldList.contains(k)) k.markColor();
		});
	}
	
	private boolean areWhiteVictorious() {
		return chess.areWhiteVictorious();
	}
	
	private boolean areBlackVictorious() {
		return chess.areBlackVictorious();
	}
	
	private void deselectJFieldList (ArrayList<JField>localJFieldList) {
		this.jFieldList.forEach((k)->{
			if (localJFieldList.contains(k)) {
			k.changeColorToDefault();}
			});
	}
	
	private void cancelSelection (ArrayList<JField>moveableJFields) {
		deselectJFieldList (moveableJFields);
		selectedJField = null;
	}
	

}
