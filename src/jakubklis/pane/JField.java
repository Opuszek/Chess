package jakubklis.pane;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

public class JField extends JButton {
	private static final long serialVersionUID = 1L;
	final private Color DefaultColor = Color.BLACK;
	final int vertical;
	final int horizontal;
	
	JField (int horizontal, int vertical) {
		super();
		this.horizontal = horizontal;
		this.vertical = vertical;
		this.setPreferredSize(new Dimension (50,50));
	}
	
	public void selectColor() {}
	
	public void changeColorToDefault() {}
	
	public void markColor() {}
	
	public int getVertical() {
		return vertical;
	}
	
	public int getHorizontal () {
		return horizontal;
	}
	
	@Override
	public boolean equals (Object other){
		if (!(other.getClass().isAssignableFrom(this.getClass()))) return false; 
	        JField otherField = (JField) other;
	        return this.getVertical()==otherField.getVertical() &&  this.getHorizontal()==otherField.getHorizontal();        
	    }
	@Override
	public String toString () {
		return "JField of horizontal value " + this.getHorizontal() + " and vertical value " + this.getVertical(); 
	}
}


