package jakubklis.pane;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JButton;

public class LightBrownField extends JField {
	
	private static final long serialVersionUID = 1L;
	final private Color DefaultColor = new Color (189,102,16);
	final private Color MarkedFieldColor = new Color (215,131,48);
	final private Color SelectedFieldColor = new Color (255,158,53);

	LightBrownField (int horizontal, int vertical) {
		super(horizontal, vertical);
		this.setBackground(DefaultColor);
		setListenersChangingColor();
		deleteBorder();
		}
	
	public void selectColor() {this.setBackground(SelectedFieldColor);}
	
	public void changeColorToDefault() {this.setBackground(DefaultColor);}
	
	public void markColor() {this.setBackground(MarkedFieldColor);}
	
	private void setListenersChangingColor () {
		this.addMouseListener(new java.awt.event.MouseAdapter() {
		    public void mouseEntered(java.awt.event.MouseEvent evt) {
		    	if (LightBrownField.this.getBackground().equals(DefaultColor))
		        LightBrownField.this.selectColor();
		    }
		    public void mouseExited(java.awt.event.MouseEvent evt) {
		    	if (LightBrownField.this.getBackground().equals(SelectedFieldColor))
		    	LightBrownField.this.changeColorToDefault();
		    }});
	}
	
	private void deleteBorder () {
		this.setBorder(BorderFactory.createEmptyBorder());
	}
	
}